export interface ProductModel {
  id?: string;
  name?: string;
  description?: string;
  stock?: number;
  price?: number;
  imgUrl?: string;
  category?: string;
  type?: string;
}
