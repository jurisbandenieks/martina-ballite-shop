import { Component, OnInit, Input } from "@angular/core";
import { PaymentService } from "src/app/basket/payment/payment.service";
import { ShopService } from "../shop.service";
import { FormControl, Validators } from "@angular/forms";

@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.scss"]
})
export class ProductComponent implements OnInit {
  @Input() product;
  amount: FormControl;

  constructor(
    private paymentService: PaymentService,
    private shopService: ShopService
  ) {}

  ngOnInit(): void {
    this.amount = new FormControl(1, [
      Validators.required,
      Validators.min(1),
      Validators.max(this.product.stock)
    ]);
  }

  addToBasket(product) {
    if (this.amount.valid) {
      this.shopService.addToBasket(product, this.amount.value);
    }
  }
}
