import { ProductModel } from "./product.model";

export interface BasketModel {
  product?: ProductModel;
  quantity?: number;
}
