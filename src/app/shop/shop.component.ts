import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth/auth.service";
import { Observable, Subscription } from "rxjs";
import { ShopService } from "./shop.service";

@Component({
  selector: "app-shop",
  templateUrl: "./shop.component.html",
  styleUrls: ["./shop.component.scss"]
})
export class ShopComponent implements OnInit {
  public allProducts;

  private shopSubscription: Subscription;

  constructor(private shopService: ShopService) {}

  ngOnInit(): void {
    this.shopSubscription = this.shopService.allProducts$.subscribe((data) => {
      this.allProducts = data;
    });
  }

  ngOnDestroy() {
    this.shopSubscription.unsubscribe();
  }
}
