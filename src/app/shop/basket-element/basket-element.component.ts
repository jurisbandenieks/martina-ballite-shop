import { Component, OnInit } from "@angular/core";
import { ShopService } from "../shop.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-basket-element",
  templateUrl: "./basket-element.component.html",
  styleUrls: ["./basket-element.component.scss"]
})
export class BasketElementComponent implements OnInit {
  public amountInBasket: number;
  private basketSubscription: Subscription;

  constructor(private shopService: ShopService) {}

  ngOnInit(): void {
    this.basketSubscription = this.shopService.basket$.subscribe((data) => {
      if (data) {
        this.amountInBasket = data.length;
      }
    });
  }

  ngOnDestroy() {
    this.basketSubscription.unsubscribe();
  }
}
