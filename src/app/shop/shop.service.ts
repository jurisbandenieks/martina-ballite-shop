import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { Observable, BehaviorSubject } from "rxjs";
import { map, tap, take, concatMap, mergeMap } from "rxjs/operators";
import { ProductModel } from "./product.model";
import { BasketModel } from "./basket.mode";
import { UtilService } from "../shared/util.service";

@Injectable({
  providedIn: "root"
})
export class ShopService {
  public allProducts$: Observable<ProductModel[]>;
  public category$ = new BehaviorSubject("");
  public categories$: Observable<any[]>;

  public basketSubject: BehaviorSubject<BasketModel[]> = new BehaviorSubject(
    null
  );
  public basket$ = this.basketSubject.asObservable();
  private basket: BasketModel[] = [];

  constructor(
    private firestore: AngularFirestore,
    private utilService: UtilService
  ) {
    this.allProducts$ = this.category$.pipe(
      mergeMap((category) => {
        return this.firestore
          .collection("products", (ref) => {
            if (category !== "") {
              return ref.where("category", "==", category);
            }
            return ref;
          })
          .snapshotChanges()
          .pipe(
            map((response) => {
              return response.map((doc) => {
                const data: any = doc.payload.doc.data();
                return { id: doc.payload.doc.id, ...data };
              });
            })
          );
      })
    );

    this.categories$ = this.firestore
      .collection("categories")
      .snapshotChanges()
      .pipe(
        map((response) => {
          return response.map((doc) => {
            const data: any = doc.payload.doc.data();
            return { id: doc.payload.doc.id, ...data };
          });
        })
      );
  }

  // Initializes cookies on app load
  initializeBasket() {
    const basketCookie = this.utilService.getCookie("basket");
    if (basketCookie) {
      this.basket = JSON.parse(basketCookie) || null;
      this.basketSubject.next(this.basket);
      console.log("Basket: ", this.basket);
    }
  }

  private setCookieState() {
    this.utilService.setCookie("basket", JSON.stringify(this.basket));
  }

  // Adds to basket, first deleting existing cookies and then replacing them
  addToBasket(product: ProductModel, quantity: number) {
    let isInBasket = false;

    this.basket.forEach((basketItem: BasketModel) => {
      if (basketItem.product.id === product.id) {
        isInBasket = true;
        basketItem.quantity = basketItem.quantity + quantity;
      }
    });

    if (!isInBasket) {
      this.basket.push({ product, quantity });
    }

    this.basketSubject.next(this.basket);
    this.setCookieState();
  }

  changeQuantity(product: BasketModel) {
    this.basket.forEach((basketItem: BasketModel) => {
      if (basketItem.product.id === product.product.id) {
        basketItem.quantity = product.quantity;
      }
    });

    this.basketSubject.next(this.basket);
    this.setCookieState();
  }

  deleteProductFromBasket(productId: string) {
    this.basket = this.basket.filter(
      (basketItem: BasketModel) => basketItem.product.id != productId
    );

    this.basketSubject.next(this.basket);
    this.setCookieState();
  }
}
