import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { NotFoundComponent } from "./not-found/not-found.component";
import { AuthGuard } from "./auth/auth.guard";
import { AuthComponent } from "./auth/auth.component";
import { ShopComponent } from "./shop/shop.component";
import { BasketComponent } from "./basket/basket.component";
import { SummaryComponent } from "./basket/summary/summary.component";
import { AdminComponent } from "./admin/admin.component";

const routes: Routes = [
  {
    path: "",
    component: ShopComponent,
    pathMatch: "full"
  },
  { path: "basket", component: BasketComponent },
  {
    path: "confirmation",
    component: SummaryComponent
  },
  {
    path: "edit",
    component: AdminComponent,
    canActivate: [AuthGuard]
  },
  { path: "admin", component: AuthComponent },
  { path: "**", component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {}
