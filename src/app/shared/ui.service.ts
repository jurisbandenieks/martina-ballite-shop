import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { MatSnackBar } from "@angular/material/snack-bar";

@Injectable({
  providedIn: "root"
})
export class UiService {
  constructor(private snackBar: MatSnackBar) {}

  showSnackbar(message, action = null, duration = 3000) {
    this.snackBar.open(message, action, { duration: duration });
  }
}
