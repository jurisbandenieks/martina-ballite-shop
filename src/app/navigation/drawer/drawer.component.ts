import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AuthService } from "src/app/auth/auth.service";
import { Observable } from "rxjs";
import { ShopService } from "src/app/shop/shop.service";

@Component({
  selector: "app-drawer",
  templateUrl: "./drawer.component.html",
  styleUrls: ["./drawer.component.scss"]
})
export class DrawerComponent implements OnInit {
  @Output() closeSidenav = new EventEmitter();
  public isAuth$: Observable<boolean>;
  public categories$ = this.shopService.categories$;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private shopService: ShopService
  ) {}

  ngOnInit(): void {
    this.isAuth$ = this.authService.isAuth$;
  }

  setCategory(category) {
    this.shopService.category$.next(category);
  }

  onClose() {
    this.closeSidenav.emit();
  }

  onLogout() {
    this.authService.logout();
    this.onClose();
  }
}
