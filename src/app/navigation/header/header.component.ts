import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { Observable, Subscription } from "rxjs";
import { AuthService } from "src/app/auth/auth.service";
import { ShopService } from "src/app/shop/shop.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  @Output() sidenavToggle = new EventEmitter();
  public isAuth$: Observable<boolean>;
  basketSubscription: Subscription;
  public amountInBasket: number = null;

  constructor(
    private authService: AuthService,
    private shopService: ShopService
  ) {}

  ngOnInit(): void {
    this.isAuth$ = this.authService.isAuth$;
    this.basketSubscription = this.shopService.basket$.subscribe((data) => {
      if (data) {
        this.amountInBasket = data.length;
      }
    });
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }

  onLogout() {
    this.authService.logout();
  }
}
