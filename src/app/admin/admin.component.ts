import { Component, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { AdminService } from "./admin.service";
import { ProductModel } from "../shop/product.model";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.scss"]
})
export class AdminComponent implements OnInit {
  public allProducts: ProductModel[];

  private shopSubscription: Subscription;

  constructor(private adminService: AdminService) {}

  ngOnInit(): void {
    this.shopSubscription = this.adminService.allProducts$.subscribe((data) => {
      this.allProducts = data;
    });
  }

  ngOnDestroy() {
    this.shopSubscription.unsubscribe();
  }

  onCancel() {
    location.reload();
  }

  onSave() {
    this.adminService.writeToDB(this.allProducts);
  }

  onAdd() {
    this.allProducts.push({
      name: "",
      price: null,
      stock: null,
      description: "",
      imgUrl: "",
      category: "",
      type: ""
    });
  }

  onDelete(product, index) {
    if (confirm("Vai tiešām vēlies dzēst produktu no katologa?")) {
      this.allProducts.splice(index, 1);
      this.adminService.deleteFromDB(product);
    }
  }
}
