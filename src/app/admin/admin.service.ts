import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ProductModel } from "../shop/product.model";
import { UtilService } from "../shared/util.service";
import { AngularFirestore } from "@angular/fire/firestore";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AdminService {
  public allProducts$: Observable<ProductModel[]>;

  constructor(
    private firestore: AngularFirestore,
    private utilService: UtilService
  ) {
    this.allProducts$ = this.firestore
      .collection("products")
      .snapshotChanges()
      .pipe(
        map((response) => {
          return response.map((doc) => {
            const data: any = doc.payload.doc.data();
            return { id: doc.payload.doc.id, ...data };
          });
        })
      );
  }

  deleteFromDB(product) {
    console.log(product);
    if (product.id) {
      this.firestore.collection("products").doc(product.id).delete();
    }
    location.reload();
  }

  writeToDB(products) {
    console.log(products);
    products.forEach((product) => {
      if (product.id) {
        console.log(product.id);
        this.firestore.collection("products").doc(product.id).set(product);
      } else {
        if (product && product.name && product.price && product.stock) {
          this.firestore.collection("products").add({
            name: product.name,
            price: Number(product.price),
            stock: Number(product.price),
            description: product.description,
            imgUrl: product.imgUrl
          });
        }
      }
    });
    location.reload();
  }
}
