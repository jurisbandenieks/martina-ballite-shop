import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthService } from "./auth.service";
import { AuthModel } from "./auth.model";

@Component({
  selector: "app-auth",
  templateUrl: "./auth.component.html",
  styleUrls: ["./auth.component.scss"]
})
export class AuthComponent implements OnInit {
  public loginData: FormGroup;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.makeForm();
  }

  onSubmit() {
    if (this.loginData.valid) {
      const loginData: AuthModel = this.loginData.value;
      this.authService.login(loginData);
    } else {
      this.authService.logout();
    }
  }

  private makeForm() {
    this.loginData = new FormGroup({
      email: new FormControl("", {
        validators: [Validators.required, Validators.email]
      }),
      password: new FormControl("", { validators: [Validators.required] })
    });
  }
}
