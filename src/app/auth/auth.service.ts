import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { BehaviorSubject } from "rxjs";
import { AuthModel } from "./auth.model";
import { Router } from "@angular/router";
import { UiService } from "../shared/ui.service";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  private isAuthSubject = new BehaviorSubject(false);
  public isAuth$ = this.isAuthSubject.asObservable();
  private isAuthenticated = false;

  constructor(
    private afAuth: AngularFireAuth,
    private uiService: UiService,
    private router: Router
  ) {}

  initAuthListener() {
    this.afAuth.authState.subscribe((user) => {
      if (user) {
        this.isAuthenticated = true;
        this.isAuthSubject.next(true);
        this.router.navigate(["/"]);
      } else {
        this.isAuthenticated = false;
        this.isAuthSubject.next(false);
      }
    });
  }

  login(authData: AuthModel) {
    this.afAuth
      .signInWithEmailAndPassword(authData.email, authData.password)
      .catch((err) => {
        this.uiService.showSnackbar(err.message);
      });
  }

  logout() {
    this.afAuth.signOut();
  }

  isAuth() {
    return this.isAuthenticated;
  }
}
