import { Component } from "@angular/core";
import { AuthService } from "./auth/auth.service";
import { Observable } from "rxjs";
import { ShopService } from "./shop/shop.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "martina-ballite-shop";

  public isAuth$: Observable<any>;

  constructor(
    private authService: AuthService,
    private shopService: ShopService
  ) {}

  ngOnInit() {
    this.authService.initAuthListener();
    this.shopService.initializeBasket();

    this.isAuth$ = this.authService.isAuth$;
  }
}
