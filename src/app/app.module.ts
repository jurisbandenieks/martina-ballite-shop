import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { MaterialModule } from "src/app/material.module";
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from "@angular/fire/firestore";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./navigation/header/header.component";
import { DrawerComponent } from "./navigation/drawer/drawer.component";
import { FooterComponent } from "./navigation/footer/footer.component";
import { AuthComponent } from "./auth/auth.component";
import { NotFoundComponent } from "./not-found/not-found.component";

import { environment } from "src/environments/environment";
import { AppRoutingModule } from "./app-routing.module";
import { ShopComponent } from "./shop/shop.component";
import { ProductComponent } from "./shop/product/product.component";
import { BasketElementComponent } from "./shop/basket-element/basket-element.component";
import { BasketComponent } from "./basket/basket.component";
import { OverviewComponent } from "./basket/overview/overview.component";
import { DetailsComponent } from "./basket/details/details.component";
import { PaymentComponent } from "./basket/payment/payment.component";
import { SummaryComponent } from "./basket/summary/summary.component";
import { ConfirmationComponent } from "./basket/confirmation/confirmation.component";
import { HttpClientModule } from "@angular/common/http";
import { AdminComponent } from './admin/admin.component';
import { EditProductComponent } from './admin/edit-product/edit-product.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DrawerComponent,
    FooterComponent,
    AuthComponent,
    NotFoundComponent,
    ShopComponent,
    ProductComponent,
    BasketElementComponent,
    BasketComponent,
    OverviewComponent,
    DetailsComponent,
    PaymentComponent,
    SummaryComponent,
    ConfirmationComponent,
    AdminComponent,
    EditProductComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MaterialModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
