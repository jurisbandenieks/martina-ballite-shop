export interface DetailsModel {
  name?: string;
  lastName?: string;
  email?: string;
  phone?: string;
  country?: string;
  city?: string;
  street?: string;
  home?: string;
  postIndex?: string;
  confirmation?: boolean;
}
