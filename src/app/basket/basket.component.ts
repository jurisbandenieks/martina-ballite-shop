import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { PaymentService } from "./payment/payment.service";
import { ShopService } from "../shop/shop.service";
import { BasketModel } from "../shop/basket.mode";
import { Subscription } from "rxjs";
import { ProductShortModel } from "./payment/checkout.model";
import { map, tap } from "rxjs/operators";

@Component({
  selector: "app-basket",
  templateUrl: "./basket.component.html",
  styleUrls: ["./basket.component.scss"]
})
export class BasketComponent implements OnInit {
  confirmationForm: FormGroup;
  detailsForm: FormGroup;

  isOngoingPayment = false;

  public basketItemsCheckout: ProductShortModel[];
  private basketSubscription: Subscription;

  constructor(
    private paymentService: PaymentService,
    private shopService: ShopService
  ) {}

  ngOnInit() {
    this.buildDetailsForm();

    this.basketSubscription = this.shopService.basket$.subscribe(
      (items: BasketModel[]) => {
        if (items) {
          this.basketItemsCheckout = items.map((item: BasketModel) => {
            return { productId: item.product.id, amount: item.quantity };
          });
        }
      }
    );
  }

  ngOnDestroy() {
    this.basketSubscription.unsubscribe();
  }

  onBuy() {
    if (this.detailsForm.valid) {
      this.isOngoingPayment = true;
      this.paymentService
        .startCourseCheckoutSession(
          this.basketItemsCheckout,
          this.detailsForm.value
        )
        .subscribe(
          (session) => {
            this.paymentService.redirectToCheckout(session);
          },
          (err) => {
            console.log("Error creating checkout session", err);
          }
        );
    }
  }

  getSum() {
    return this.shopService.basket$.pipe(
      map((items) => {
        let sum = 0;
        items.forEach((item) => {
          sum += Number(item.product.price) * Number(item.quantity);
        });
        return sum;
      })
    );
  }

  onValidateForms() {
    this.validateAllFormFields(this.detailsForm);
  }

  private buildDetailsForm() {
    this.detailsForm = new FormGroup({
      name: new FormControl("Test", [Validators.required]),
      lastName: new FormControl("User", [Validators.required]),
      email: new FormControl("test@test.com", [Validators.required]),
      phone: new FormControl("12345678", [Validators.required]),
      country: new FormControl("Latvija", [Validators.required]),
      city: new FormControl("Rīga", [Validators.required]),
      street: new FormControl("Test", [Validators.required]),
      streetNumber: new FormControl("0", [Validators.required]),
      postIndex: new FormControl("1000", [Validators.required]),
      confirmation: new FormControl(false, [Validators.requiredTrue])
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
