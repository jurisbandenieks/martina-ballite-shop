import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { filter, first, tap } from "rxjs/operators";
import { CheckoutSession } from "./checkout-session.model";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { ProductShortModel } from "./checkout.model";
import { UtilService } from "src/app/shared/util.service";
import { ShopService } from "src/app/shop/shop.service";
import { DetailsModel } from "../details/details.model";

declare const Stripe;

@Injectable({
  providedIn: "root"
})
export class PaymentService {
  constructor(
    private http: HttpClient,
    private firestore: AngularFirestore,
    private utilService: UtilService,
    private shopService: ShopService
  ) {}

  startCourseCheckoutSession(
    checkoutData: ProductShortModel[],
    customerData: DetailsModel
  ): Observable<CheckoutSession> {
    const productsString = JSON.stringify(checkoutData);
    const customerString = JSON.stringify(customerData);

    return this.http.post<CheckoutSession>(
      environment.api.baseUrl + "/api/checkout",
      {
        productsString,
        customerString,
        callbackUrl: this.buildCallbackUrl()
      }
    );
  }

  buildCallbackUrl() {
    const protocol = window.location.protocol,
      hostname = window.location.hostname,
      port = window.location.port;

    let callbackUrl = `${protocol}//${hostname}`;

    if (port) {
      callbackUrl += `:${port}`;
    }

    callbackUrl += "/confirmation";

    return callbackUrl;
  }

  redirectToCheckout(session: CheckoutSession) {
    const stripe = Stripe(session.stripePublicKey);

    stripe.redirectToCheckout({
      sessionId: session.stripeCheckoutSessionId
    });
  }

  waitForPurchaseCompleted(ongoingPurchaseSessionId: string): Observable<any> {
    return this.firestore
      .doc<any>(`purchaseSessions/${ongoingPurchaseSessionId}`)
      .valueChanges()
      .pipe(
        filter((purchase) => {
          if (purchase.status == "completed") {
            this.utilService.deleteCookie("basket");
            return purchase;
          }
        }),
        first()
      );
  }
}
