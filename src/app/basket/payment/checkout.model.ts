import { DetailsModel } from "../details/details.model";

export interface ProductShortModel {
  productId?: string;
  amount?: number;
}

export interface CheckoutModel {
  products?: ProductShortModel[];
  details?: DetailsModel;
}
