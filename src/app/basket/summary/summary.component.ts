import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UtilService } from "src/app/shared/util.service";
import { ShopService } from "src/app/shop/shop.service";
import { PaymentService } from "../payment/payment.service";

@Component({
  selector: "app-summary",
  templateUrl: "./summary.component.html",
  styleUrls: ["./summary.component.scss"]
})
export class SummaryComponent implements OnInit {
  @Input() formData;

  public message;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private utilService: UtilService,
    private shopService: ShopService,
    private paymentService: PaymentService
  ) {}

  ngOnInit(): void {
    const result = this.route.snapshot.queryParamMap.get("purchaseResult");

    if (result == "success") {
      const ongoingPurchaseSessionId = this.route.snapshot.queryParamMap.get(
        "ongoingPurchaseSessionId"
      );

      const protocol = window.location.protocol,
        hostname = window.location.hostname,
        port = window.location.port;

      let callbackUrl = `${protocol}//${hostname}`;

      if (port) {
        callbackUrl += `:${port}`;
      }

      this.paymentService
        .waitForPurchaseCompleted(ongoingPurchaseSessionId)
        .subscribe(() => {
          // this.waiting = false;
          this.message = "Pirkums veiksmīgs, atpakaļ uz veikalu...";
          setTimeout(() => (location.href = callbackUrl), 3000);
        });
    } else {
      // this.waiting = false;
      this.message = "Pirkums nav veiksmīgs, atpakaļ uz veikalu...";
      setTimeout(() => this.router.navigateByUrl("/"), 3000);
    }
  }
}
