import { Component, OnInit } from "@angular/core";
import { ShopService } from "src/app/shop/shop.service";
import { BasketModel } from "src/app/shop/basket.mode";
import { Subscription } from "rxjs";
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-overview",
  templateUrl: "./overview.component.html",
  styleUrls: ["./overview.component.scss"]
})
export class OverviewComponent implements OnInit {
  displayedColumns: string[] = [
    "name",
    "price",
    "quantity",
    "actions",
    "priceSum"
  ];
  public basketItems: BasketModel[];
  private basketSubscription: Subscription;

  public quantity: FormControl;

  constructor(private shopService: ShopService) {}

  ngOnInit(): void {
    this.basketSubscription = this.shopService.basket$.subscribe(
      (items: BasketModel[]) => {
        this.basketItems = items;
      }
    );
  }

  ngOnDestroy() {
    this.basketSubscription.unsubscribe();
  }

  onDelete(item: BasketModel) {
    this.shopService.deleteProductFromBasket(item.product.id);
  }

  onChangeQuantity(item) {
    this.shopService.changeQuantity(item);
  }

  getSum() {
    let sum = 0;
    this.basketItems.forEach((item) => {
      sum += item.product.price * item.quantity;
    });

    return Number(sum.toFixed(2));
  }
}
