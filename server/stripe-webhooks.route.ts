import { Request, Response } from "express";
import { getDocData, db } from "./database";

const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

export async function stripeWebhooks(req: Request, res: Response) {
  try {
    const signature = req.headers["stripe-signature"];

    const event = stripe.webhooks.constructEvent(
      req.body,
      signature,
      process.env.STRIPE_WEBHOOK_SECRET
    );

    if (event.type == "checkout.session.completed") {
      const session = event.data.object;
      await onCheckoutSessionCompleted(session);
    }

    res.json({ received: true });
  } catch (err) {
    console.log("Error processing webhook event, reason: ", err);
    return res.status(400).send(`Webhook Error: ${err.message}`);
  }
}

async function onCheckoutSessionCompleted(session) {
  const purchaseSessionId = session.client_reference_id;

  const sessionRef = await getDocData(`purchaseSessions/${purchaseSessionId}`);

  if (sessionRef) {
    await fulfillCoursePurchase(purchaseSessionId);
  }
}

async function fulfillCoursePurchase(purchaseSessionId: string) {
  const batch = db.batch();

  const purchaseSessionRef = db.doc(`purchaseSessions/${purchaseSessionId}`);

  const purchaseSessionData = await getDocData(
    `purchaseSessions/${purchaseSessionId}`
  );

  const products = purchaseSessionData.products;

  for await (let product of products) {
    const productSessionRef = db.doc(`products/${product.id}`);

    const productItem = await getDocData(`products/${product.id}`);

    batch.update(productSessionRef, {
      stock: productItem.stock - product.amount
    });
  }

  batch.update(purchaseSessionRef, { status: "completed" });

  return batch.commit();
}
