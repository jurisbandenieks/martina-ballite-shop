import { Request, Response } from "express";
import { getDocData, db } from "./database";
import { Timestamp } from "@google-cloud/firestore";

interface ProductShortModel {
  productId?: string;
  amount?: number;
}

interface RequestInfo {
  products: ProductShortModel[];
  customer: DetailsModel;
  callbackUrl: string;
}

interface Product {
  id?: string;
  name?: string;
  price?: number;
  description?: string;
  imgUrl?: string;
  stock?: number;
  amount?: number;
}

interface DetailsModel {
  name?: string;
  lastName?: string;
  email?: string;
  phone?: string;
  country?: string;
  city?: string;
  street?: string;
  home?: string;
  postIndex?: string;
  confirmation?: boolean;
}

const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);
const products: Product[] = [];

export async function createCheckoutSession(req: Request, res: Response) {
  try {
    const info: RequestInfo = {
      products: JSON.parse(req.body.productsString),
      customer: JSON.parse(req.body.customerString),
      callbackUrl: req.body.callbackUrl
    };

    const purchaseSession = await db.collection("purchaseSessions").doc();

    let sessionConfig;

    if (info.products) {
      for await (let product of info.products) {
        const productItem = await getDocData(`products/${product.productId}`);
        products.push({
          ...productItem,
          id: product.productId,
          amount: product.amount
        });
      }

      if (products) {
        const checkoutSessionData: any = {
          status: "ongoing",
          created: Timestamp.now(),
          products: products,
          customer: info.customer
        };

        await purchaseSession.set(checkoutSessionData);

        sessionConfig = setupPurchaseCourseSession(
          info,
          products,
          purchaseSession.id
        );

        const session = await stripe.checkout.sessions.create(sessionConfig);

        res.status(200).json({
          stripeCheckoutSessionId: session.id,
          stripePublicKey: process.env.STRIPE_PUBLIC_KEY
        });
      }
    }
  } catch (error) {
    res.status(500).json({ error: "Could not initiate session" });
  }
}

function setupPurchaseCourseSession(
  info: RequestInfo,
  products: Product[],
  sessionId: string
) {
  const config = setupBaseSessionConfig(info, sessionId);

  const configItems = [];

  products.forEach((product) => {
    configItems.push({
      name: product.name,
      description: product.description,
      amount: product.price * 100,
      currency: "eur",
      quantity: product.amount
    });
  });

  return { ...config, line_items: configItems };
}

function setupBaseSessionConfig(info: RequestInfo, sessionId: string) {
  const config: any = {
    payment_method_types: ["card"],
    success_url: `${info.callbackUrl}?purchaseResult=success&ongoingPurchaseSessionId=${sessionId}`,
    cancel_url: `${info.callbackUrl}?purchaseResult=failed`,
    client_reference_id: sessionId
  };

  return config;
}
